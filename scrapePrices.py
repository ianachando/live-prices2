import requests
import bs4 as bs
import mysql.connector
import schedule
import time


mydb = mysql.connector.connect(
  host="",
  user="",
  password="",
  database="",
)

mycursor = mydb.cursor()

url = "https://tickers.mystocks.co.ke/ticker/RMWX$?app=FIB;f=mslFrame0;d=fib.co.ke"
page = requests.get(url)
file = bs.BeautifulSoup(page.text, features="xml")

stocks = file.find_all("i")

#create equity table
# mycursor.execute((
#     "  CREATE TABLE `equitytest2` ("
#     "  `code` varchar(25) NOT NULL,"
#     "  `open` varchar(25) NOT NULL,"
#     "  `price` varchar(25) NOT NULL,"
#     "  `high` varchar(25) NOT NULL,"
#     "  `low` varchar(25) NOT NULL,"
#     "  `close` varchar(25) NOT NULL,"
#     "  `volume` varchar(25) NOT NULL,"
#     "  `change_price` varchar(25) NOT NULL,"
#     "  `change_percentage` varchar(25) NOT NULL,"
#     "  PRIMARY KEY (`code`))"))

#Insert New Stock
# insert_new_stock = (
#   "INSERT INTO equitytest2 (code, open, price, high, low, close, volume, change_price, change_percentage) "
#   "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)")

# for stock in stocks:
#     stock_symbol = stock["a"],
#     mycursor.execute(insert_new_stock, (stock_symbol[0], 10, 10, 10, 10, 10, 10, 10, 10, ))
#     mydb.commit()

#     print(stock_symbol[0])


#Update stock details

def update_live_prices(): 
  update_stock_details =  (
  "UPDATE equitytest2 SET open = %s, price = %s, high = %s, low = %s, close = %s, volume =  %s, price = %s, change_percentage = %s "
  "WHERE code = %s")


  for stock in stocks:
      if stock["d"] == "-":
          change = "0"
      else:
          change = stock["d"]
    
      if len(stock["e"]) > 0:
          change_percentage = stock["e"][:-1]
      else:
          change_percentage = "0"

  
      stock_symbol = stock["a"],
      prev_price = stock["b"]
      latest_price = stock["c"],
      change = stock["d"],
      change_percentage = stock["e"],
      days_high = stock["f"],
      days_low = stock["g"],
      volume_traded = stock["h"],
      average_price = stock["j"],

      if change[0] == "-":
          change_price = "0"
      else:
          change_price = change[0]

      if days_high[0] == "-":
          high = "0"
      else:
          high = days_high[0]
      
      if days_low[0] == "-":
          low = "0"
      else:
          low = days_low[0]
      
      if volume_traded[0] == "-":
          volume = "0"
      else:
          volume = volume_traded[0]
      
      if len(change_percentage[0]) > 0:
          price_change_percentage = change_percentage[0][:-1]
      else:
          price_change_percentage[0] = "0"

      
      mycursor.execute(update_stock_details, (prev_price, latest_price[0], high, low, latest_price[0], volume, change_price, price_change_percentage, stock_symbol[0]))
      mydb.commit()

      print(stock_symbol[0], prev_price, latest_price[0], high, low, latest_price[0], volume, change_price, price_change_percentage)


schedule.every(1).minutes.do(update_live_prices)
  
while True:
    schedule.run_pending()
    time.sleep(1)

